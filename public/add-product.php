<?php require_once('../vendor/autoload.php'); ?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>

    <div class="container">
        <div class="row justify-content-between mt-3 pb-2 border-bottom border-bottom-3 border-dark">
            <div class="col-4">
                <h3>Product Add</h3>
            </div>

            <div class="col-4 d-flex justify-content-end">
                <button id="submitAddProductForm" href="add-product.php" class="btn btn-primary me-1">Save</button>
                <a href="index.php" class="btn btn-danger">Cancel</a>
            </div>
        </div>

        <!-- List of Products -->
        <div class="row g-4 my-3 pb-4 justify-content-center border-bottom border-bottom-3 border-dark">
            <div class="col-8">
                <form id="addProductForm" novalidate>

                    <div class="row justify-content-center mb-3">
                        <div class="col-9 text-center text-danger justify-content-center" id="err-msg">
                            <h3 id="msg">All fields are required</h3>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label for="sku" class="col-sm-2 col-form-label">SKU</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="sku" id="sku" required>
                            <div class="invalid-feedback" id="sku-msg">
                                Please Enter an SKU.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="name" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name" required>
                            <div class="invalid-feedback">
                                Please Enter a product name.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="price" class="col-sm-2 col-form-label">Price ($)</label>
                        <div class="col-sm-10">
                            <input type="number" step="0.01" name="price" class="form-control" min="0" id="price"
                                required>
                            <div class="invalid-feedback">
                                Please enter a product price.
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label for="switcher" class="col-sm-2 col-form-label">Type Switcher</label>
                        <div class="col-sm-10">
                            <select class="form-select" name="type-switcher" id="type-switcher" required>
                                <option value="DVD">DVD</option>
                                <option value="Book">Book</option>
                                <option value="Furniture">Furniture</option>
                            </select>
                        </div>
                    </div>

                    <div id="type-attribute">
                        <div class="row mb-3">
                            <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
                            <div class="col-sm-10">
                                <input type="number" step="0.01" name="size" class="form-control" id="size" required>
                                <div class="invalid-feedback">
                                    Please provide a size.
                                </div>
                                <small>Please Provide the size of the DVD in MB</small>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <footer>
            <div class="row">
                <div class="col text-center">
                    <h5 class="mt-0">Scandiweb Test assignment</h5>
                </div>
            </div>
        </footer>
    </div>

    <script src="js/add-product.js"></script>

    <script>
    </script>

</body>

</html>