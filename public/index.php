<?php

require_once('../vendor/autoload.php');

use App\Factory\ProductFactory;
use App\Products\Product;

$productTypes = ProductFactory::getProductType();

if (isset($_POST['product_ids'])) {
    Product::deleteProductsById($_POST['product_ids']);
}

$allProducts = Product::getAllProducts();


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product List</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <div class="container">
        <div class="row justify-content-between mt-3 pb-2 border-bottom border-bottom-3 border-dark">
            <div class="col-4">
                <h3>Product List</h3>
            </div>

            <div class="col-4 d-flex justify-content-end">
                <a href="add-product.php" class="btn btn-primary me-1">Add</a>
                <button type="button" class="btn btn-danger" id="mass-delete">Mass Delete</button>
            </div>
        </div>

        <!-- List of Products -->
        <form method="POST" id="deleteProductForm">

            <div class="row row-cols-1 row-cols-md-4 g-4 my-3 pb-4 border-bottom border-bottom-3 border-dark"
                id="product-list">
                <?php foreach ($allProducts as $product) { ?>
                <?php $specificAttribute = $productTypes[$product->type]::getSpecificAttribute($product->specific_attribute); ?>
                <div class="col">
                    <div class="card text-center position-relative">
                        <div class="card-body my-3">
                            <input name="product_ids[]" type="checkbox" value="<?php echo $product->product_id ?>"
                                class="checkbox position-absolute delete">
                            <h5 class="card-title"><?php echo $product->sku ?></h5>
                            <p class="card-text"><?php echo $product->name ?></p>
                            <p class="card-text"><?php echo number_format($product->price, 2) ?> $</p>
                            <p class="card-text"><?php echo $specificAttribute ?></p>
                        </div>
                    </div>
                </div>
                <?php } ?>

            </div>

        </form>


        <footer>
            <div class="row">
                <div class="col text-center">
                    <h5 class="mt-0">Scandiweb Test assignment</h5>
                </div>
            </div>
        </footer>
    </div>

    <script>
    document.querySelector('#mass-delete').addEventListener('click', function(e) {
        e.preventDefault();
        document.querySelector('#deleteProductForm').submit();
    });
    </script>

</body>

</html>