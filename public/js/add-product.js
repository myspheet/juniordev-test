(function() {
    const init = () => {
        document
            .querySelector("#submitAddProductForm")
            .addEventListener("click", submitProductForm);

        document
            .querySelector("#type-switcher")
            .addEventListener("change", (e) => {
                const typeAttributeDiv = document.querySelector(
                    "#type-attribute"
                );
                typeAttributeDiv.innerHTML = "";
                typeAttributeDiv.innerHTML =
                    Form.ProductTypeFuncMap[e.target.value];
            });
    };

    const submitProductForm = (e) => {
        e.preventDefault();

        const form = document.querySelector("#addProductForm");
        if (!form.checkValidity || form.checkValidity()) {
            /* submit the form */
            // form.submit();
            const formData = new FormData(form);
            // formData.append(Object.fromEntries(formData.entries()));
            fetch("add.php", {
                    method: "POST",
                    credentials: "same-origin",
                    body: formData,
                })
                .then((response) => {
                    if (!response.ok) {
                        return response.text().then((text) => {
                            throw new Error(text);
                        });
                    }
                    return response.json();
                })
                .then((response) => {
                    console.log(response);
                    window.location = "index.php";
                })
                .catch((err) => {
                    console.log(err);
                    const message = JSON.parse(err["message"]).error;
                    const skuMsg = document.querySelector(
                        `#${message.field}-msg`
                    );
                    skuMsg.style.display = "block";
                    skuMsg.innerText = message.message;
                    form.classList.add("was-validated");

                    document.querySelector("#err-msg").style.display = "block";
                });

            return;
        }
        e.stopPropagation();

        form.classList.add("was-validated");

        document.querySelector("#err-msg").style.display = "block";
    };

    init();
})();

class Form {
    static ProductTypeFuncMap = {
        Furniture: this.addFurnitureFormFields(),
        Book: this.addBookFormFields(),
        DVD: this.addDVDFormFields(),
    };

    static addBookFormFields() {
        return `
          <div class="row mb-3">
            <label for="weight" class="col-sm-2 col-form-label">Weight (KG)</label>
            <div class="col-sm-10">
              <input type="number" step="0.01" min="0" name="weight" class="form-control" id="weight" required>
              <div class="invalid-feedback">
                  Please provide a weight.
              </div>
              <small>Please Provide the weight of the Book in (KG)</small>
            </div>
          </div>
          `;
    }

    static addFurnitureFormFields() {
        return `
          <div class="row mb-3">
            <label for="height" class="col-sm-2 col-form-label">Height (CM)</label>
            <div class="col-sm-10">
              <input type="number" step="0.01" min="0" name="height" class="form-control" id="height" required>
              <div class="invalid-feedback">
              Please provide a height.
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <label for="width" class="col-sm-2 col-form-label">Width (CM)</label>
            <div class="col-sm-10">
              <input type="number" step="0.01" min="0" name="width" class="form-control" id="width" required>
              <div class="invalid-feedback">
              Please provide a width.
              </div>
            </div>
          </div>
          <div class="row mb-3">
            <label for="length" class="col-sm-2 col-form-label">Length (CM)</label>
            <div class="col-sm-10">
              <input type="number" step="0.01" min="0" name="length" class="form-control" id="length" required>
              <div class="invalid-feedback">
                  Please provide a length.
              </div>
              <small>Please Provide Dimensions in HxWxL</small>
            </div>
          </div>
          `;
    }

    static addDVDFormFields() {
        return `
          <div class="row mb-3">
            <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
            <div class="col-sm-10">
              <input type="number" step="0.01" min="0" name="size" class="form-control" id="size" required>
              <div class="invalid-feedback">
                  Please provide a size.
              </div>
              <small>Please Provide the size of the DVD in (MB)</small>
            </div>
          </div>
          `;
    }
}