<?php

require('../vendor/autoload.php');

use App\Factory\ProductFactory;
use App\Products\Product;

$productTypes = ProductFactory::getProductType();

$product = new $productTypes[$_POST['type-switcher']]($_POST);

if (Product::getProductBySKU($product->getSku())) {
    http_response_code(400);
    echo json_encode(['error' => ["field" => "sku", "message" => "A product with the sku {$product->getSku()} already exists"]]);
} else {
    $product->save();
    http_response_code(200);
    echo json_encode(['message' => 'Success']);
}