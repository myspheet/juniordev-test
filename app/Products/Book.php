<?php

namespace App\Products;

use App\Utils\Database;
use DateTime;

class Book extends Product
{
    protected $weight;

    public function __construct(array $args)
    {
        parent::__construct($args);
        $this->weight = $args['weight'];
    }


    // This returns the specific attribute for the book product
    public static function getSpecificAttribute($attr): string
    {
        $attr = json_decode($attr);
        return "Weight: {$attr->weight}KG";
    }

    // implementation of abstract method to save a product
    public function save(): void
    {
        $statment = "INSERT INTO products(name, sku, price, type, specific_attribute) VALUES(:name, :sku, :price, :type , :specificAttribute)";
        $this->dbConn->insert($statment, [
            'name' => $this->name, 'sku' => $this->sku, 'price' => (float) $this->price,
            'type' => $this->type,
            'specificAttribute' => json_encode(['weight' => $this->weight])
        ]);
    }
}