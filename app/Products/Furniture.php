<?php

namespace App\Products;

class Furniture extends Product
{
    protected $length;
    protected $height;
    protected $width;

    public function __construct(array $args)
    {
        parent::__construct($args);
        $this->length = $args['length'];
        $this->height = $args['height'];
        $this->width = $args['width'];
    }

    // implementation of abstract method to save a product
    public function save(): void
    {
        $statment = "INSERT INTO products(name, sku, price, type, specific_attribute) VALUES(:name, :sku, :price, :type , :specificAttribute)";
        $this->dbConn->insert($statment, [
            'name' => $this->name, 'sku' => $this->sku, 'price' => (float) $this->price, 'type' => $this->type,
            'specificAttribute' => json_encode(['height' => $this->height, 'length' => $this->length, 'width' => $this->width])
        ]);
    }

    // This returns the specific attribute for the furniture product
    public static function getSpecificAttribute($attr): string
    {

        $attr = json_decode($attr);
        $length = $attr->length;
        $height = $attr->height;
        $width = $attr->width;
        return "Dimension: {$height}x{$width}x{$length}";
    }
}