<?php

namespace App\Products;

use App\Utils\Database;

abstract class Product
{

    protected $name;
    protected $sku;
    protected $price;
    protected $type;
    protected $dbConn;

    public function __construct(array $args)
    {
        $this->name = $args['name'];
        $this->price = $args['price'];
        $this->sku = $args['sku'];
        $this->type = $args['type-switcher'];
        $this->dbConn = new Database();
    }

    // Let each child product handle their own save functionality
    abstract public function save(): void;

    // Let each child product handle the return of the specific attribute display format
    abstract public static function getSpecificAttribute($attr): string;


    public static function getProductBySKU($sku)
    {
        $statment = "SELECT * FROM products WHERE sku=:sku";
        $dbConn = new Database();
        return $dbConn->select($statment, ['sku' => $sku]);
    }

    public static function getAllProducts()
    {
        $statment = "SELECT * FROM products";
        $dbConn = new Database();
        return $dbConn->select($statment);
    }

    public static function deleteProductsById($ids)
    {
        $dbConn = new Database();
        $query = "DELETE FROM products WHERE product_id=:id";

        foreach ($ids as $id) {
            $dbConn->delete($query, ['id' => $id]);
        }
    }

    public function getSku()
    {
        return $this->sku;
    }
    public function getName()
    {
        return $this->name;
    }
    public function getPrice()
    {
        return $this->price;
    }
}