<?php

namespace App\Products;

class DVD extends Product
{
    protected $size;

    public function __construct($args)
    {
        parent::__construct($args);
        $this->size = $args['size'];
    }

    // implementation of abstract method to save a product
    public function save(): void
    {
        $statment = "INSERT INTO products(name, sku, price, type, specific_attribute) VALUES(:name, :sku, :price, :type , :specificAttribute)";
        $this->dbConn->insert($statment, [
            'name' => $this->name, 'sku' => $this->sku, 'price' => (float) $this->price,
            'type' => $this->type,
            'specificAttribute' => json_encode(['size' => $this->size])
        ]);
    }

    // This returns the specific attribute for the DVD product
    public static function getSpecificAttribute($attr): string
    {
        $attr = json_decode($attr);
        return "Size: {$attr->size}MB";
    }
}