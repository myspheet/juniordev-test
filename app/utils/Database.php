<?php

namespace App\Utils;

use PDO;
use Exception;

class Database
{
    private $connection;

    // this function is called everytime this class is instantiated
    public function __construct($dbhost = "remotemysql.com", $dbname = "tA1nhyyBkK", $username = "tA1nhyyBkK", $password = "Nrfzz6aPz6")
    {

        try {

            $this->connection = new PDO("mysql:host={$dbhost};dbname={$dbname};", $username, $password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
            $this->connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Insert a row/s in a Database Table
    public function insert($statement = "", $parameters = [])
    {
        try {

            $this->executeStatement($statement, $parameters);
            return $this->connection->lastInsertId();
        } catch (\PDOException $e) {
            if ($e->getCode() == 1062) {
                throw new Exception('Integrity Constraint');
            }
        }
    }

    // Select a row/s in a Database Table
    public function select($statement = "", $parameters = [])
    {
        try {

            $stmt = $this->executeStatement($statement, $parameters);
            return $stmt->fetchAll();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    // Remove a row/s in a Database Table
    public function delete($statement = "", $parameters = [])
    {
        try {

            $this->executeStatement($statement, $parameters);
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }


    private function executeStatement($statement = "", $parameters = [])
    {
        try {

            $stmt = $this->connection->prepare($statement);
            $stmt->execute($parameters);
            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
}