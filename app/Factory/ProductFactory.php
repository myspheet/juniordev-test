<?php

namespace App\Factory;


class ProductFactory
{
    static protected $productType = [];

    static public function loadAllProductTypes($path = '../app/Products', $superClass = 'Product', $productNamespace = 'App\Products')
    {
        $files = array_slice(scandir($path), 2);
        $productTypes = [];
        foreach ($files as $file) {

            $name = explode('.', $file)[0];
            if ($name === $superClass) {
                continue;
            }
            $productTypes[$name] = "$productNamespace\\$name";
        }

        return $productTypes;
    }

    static public function getProductType()
    {
        self::$productType = self::loadAllProductTypes();
        return self::$productType;
    }
}